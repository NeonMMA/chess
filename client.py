"""
Файл клиента.
Первая часть принимает сообщения с сервера и меняет параметры.
Вторая - рисовка доски + обработка логики игры.
"""

import socket
import json
from threading import Thread
import pygame
from all_function import board_gui
from all_function import draw_pieces
from all_function import change_player
from all_function import draw_frame_und_motion
from all_function import check_king
from all_function import new_game_board
from all_function import reload_kill
from all_function import check_all_kill


# сливаем код из clienta сюда
SERVER = "127.0.0.1"
PORT = 1488
MESSAGE = "hi"
NEWBOARD = False
BOARDSIZE = 8
GAMEBOARD = [[None] * BOARDSIZE for _ in range(BOARDSIZE)]

# подключаемся к серваку
client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    client.connect((SERVER, PORT))
except ConnectionError:
    print("сервер умер")


# приход с сервера
def serv_mess():
    """
    Цикл слушателя сообщений с сервера.
    """
    while True:
        in_data = client.recv(4096)
        global GAMEBOARD
        global MOVEMENT
        global PLAYER
        global NEWBOARD
        data_arr = json.loads(in_data)
        serv_game_board = data_arr[0]
        if serv_game_board != GAMEBOARD:
            GAMEBOARD = serv_game_board
            NEWBOARD = True

        PLAYER = data_arr[1]
        MOVEMENT = data_arr[2]
        # print("from serv: ", in_data.decode())


def set_message(new_mess):
    """
    функция изменения сообщения для отправки на сервер
    """
    global MESSAGE
    MESSAGE = new_mess


def sender():
    """
    функция отправки на сервер
    """
    if MESSAGE == "hi":
        message = "1"

    while True:
        if message != MESSAGE:
            message = MESSAGE
            client.sendall(bytes(message, "UTF-8"))
            # print("sender: " + str(message))


t1 = Thread(target=sender)
t1.start()
t2 = Thread(target=serv_mess)
t2.start()

NEWGAME = True
PLAYER = "red"

BLACK = (45, 19, 0)
WHITE = (226, 211, 180)
RED = (255, 0, 0)
GREYBACKGROUND = (252, 245, 228)

WIDTH = 70
HEIGHT = 70
RADIUS = 30
MARGIN = 0

# отступ от границы окна слева
XDISTANCEFROMEDGE = 230
killBoard = [[None] * BOARDSIZE for _ in range(BOARDSIZE)]
windowSize = [BOARDSIZE * HEIGHT + (BOARDSIZE + 1) * MARGIN + XDISTANCEFROMEDGE * 2, BOARDSIZE * HEIGHT + (BOARDSIZE + 1) * MARGIN]
# где стоит шашка которая срубила
killer_queen = []

pygame.init()

# вывод главного окна + его название
screen = pygame.display.set_mode(windowSize)
pygame.display.set_caption(" Не баг, а фича ")
DONE = False
clock = pygame.time.Clock()

# рисуем первый раз пустой холст
if not DONE:
    #  заливка заднего фона вокруг доски
    screen.fill(GREYBACKGROUND)

    # рисуем черные прямоугольники на белом фоне
    board_gui(MARGIN, WIDTH, HEIGHT, WHITE, BLACK)

    # рисуем стоковую таблицу
    if NEWGAME:
        new_game_board(GAMEBOARD, BLACK, WHITE)

    draw_pieces(GAMEBOARD, BLACK, RED, MARGIN, WIDTH, HEIGHT, WHITE, RADIUS)
    clock.tick(60)
    pygame.display.flip()

while not DONE:

    screen.fill(GREYBACKGROUND)
    board_gui(MARGIN, WIDTH, HEIGHT, WHITE, BLACK)
    draw_pieces(GAMEBOARD, BLACK, RED, MARGIN, WIDTH, HEIGHT, WHITE, RADIUS)
    COLUMN = None
    MOTION = False

    if NEWBOARD:
        killBoard = reload_kill(killBoard, GAMEBOARD, PLAYER)
        NEWBOARD = False
        pygame.display.flip()

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            DONE = True
        elif event.type == pygame.MOUSEBUTTONDOWN and MOVEMENT:

            # забрали координаты мыши
            pos = pygame.mouse.get_pos()

            # координаты мыши в номерах колонок и строк
            COLUMN = (pos[0] - XDISTANCEFROMEDGE) // (WIDTH + MARGIN)
            row = pos[1] // (HEIGHT + MARGIN)

            # написать условие, если надо рубить,то светить только рубку
            if not killer_queen:
                # проверка, нужно ли рубить
                if check_all_kill(killBoard):
                    if killBoard[COLUMN][row]:
                        # рубить, надо только рубить
                        draw_frame_und_motion(
                            GAMEBOARD, COLUMN, row, PLAYER, MARGIN, WIDTH, HEIGHT, True
                        )
                else:
                    # никого не трогаем, просто ходим
                    draw_frame_und_motion(
                        GAMEBOARD, COLUMN, row, PLAYER, MARGIN, WIDTH, HEIGHT, False
                    )
            else:
                # рубим только той шашкой, которая ходила
                draw_frame_und_motion(
                    GAMEBOARD,
                    killer_queen[0],
                    killer_queen[1],
                    PLAYER,
                    MARGIN,
                    WIDTH,
                    HEIGHT,
                    True,
                )

            # если то, куда нажали является массивом возможных ходов шашки
            if (
                COLUMN in range(BOARDSIZE)
                and row in range(BOARDSIZE)
                and isinstance(GAMEBOARD[COLUMN][row], list) # проверка на тип данных
            ):

                # рубим шашку если она была под нами (если второе поле - не пустой массив)
                if GAMEBOARD[COLUMN][row][1] != []:
                    GAMEBOARD[GAMEBOARD[COLUMN][row][1][0]][
                        GAMEBOARD[COLUMN][row][1][1]
                    ] = None

                    # "ход" шашки
                    # значение шашки на старой позиции
                    # (висит в текущей точке в первом(0) подмассиве)
                    vart = GAMEBOARD[GAMEBOARD[COLUMN][row][0][0]][
                        GAMEBOARD[COLUMN][row][0][1]
                    ]
                    GAMEBOARD[GAMEBOARD[COLUMN][row][0][0]][
                        GAMEBOARD[COLUMN][row][0][1]
                    ] = None  # где была шашка в ноль
                    GAMEBOARD[COLUMN][
                        row
                    ] = vart  # в текущую позици -> старую позицию шашки
                    check_king(GAMEBOARD)
                    MOTION = True

                    # если срубили - ходим еще раз
                    killBoard = reload_kill(killBoard, GAMEBOARD, PLAYER)
                    if killBoard[COLUMN][row]:
                        killer_queen = [COLUMN, row]  # где стоит шашка которая срубила
                    else:
                        killer_queen = []

                else:
                    # "ход" шашки
                    # значение шашки на старой позиции
                    # (висит в текущей точке в первом(0) подмассиве)
                    vart = GAMEBOARD[GAMEBOARD[COLUMN][row][0][0]][
                        GAMEBOARD[COLUMN][row][0][1]
                    ]
                    GAMEBOARD[GAMEBOARD[COLUMN][row][0][0]][
                        GAMEBOARD[COLUMN][row][0][1]
                    ] = None  # где была шашка в ноль
                    GAMEBOARD[COLUMN][
                        row
                    ] = vart  # в текущую позици -> старую позицию шашки
                    check_king(GAMEBOARD)
                    MOTION = True

                if not killer_queen:
                    # если срубили  и не можем дальше или просто сходили -> поменяли игрока
                    PLAYER = change_player(PLAYER)
                    MOVEMENT = not MOVEMENT
                    # передаем новую доску, нового игрока, возможность ходить
                    set_message(json.dumps([GAMEBOARD, PLAYER, not MOVEMENT]))

            # обновляет содержимое основного окна игры
            pygame.display.flip()

    # указывается количество смены кадров в секунду
    clock.tick(60)

    # если подвинулась шашка, то перерисуем экран
    if MOTION:
        screen.fill(GREYBACKGROUND)
        board_gui(MARGIN, WIDTH, HEIGHT, WHITE, BLACK)
        draw_pieces(GAMEBOARD, BLACK, RED, MARGIN, WIDTH, HEIGHT, WHITE, RADIUS)
        pygame.display.flip()

pygame.quit()
