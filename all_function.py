"""
Данный файл содержит почти все функции необходимые в проекте.
"""
import pygame

black = (45, 19, 0)
WHITE = (226, 211, 180)

# отступ от границы окна слева
xDistanceFromEdge = 230
BOARDSIZE = 8
CHESSROW = 3
gameBoard = [[None] * BOARDSIZE for _ in range(BOARDSIZE)]
windowSize = [960, 640]

pygame.init()

# вывод главного окна + его название
screen = pygame.display.set_mode(windowSize)
pygame.display.set_caption(" Не баг, а фича ")
clock = pygame.time.Clock()


def change_player(player):
    """
    Смена ходящей стороны
    :param player: Какой игрок ходил сейчас
    :return: Какой игрок ходит теперь
    """
    return "red" if player == "black" else "black"


def square_colour(col, row, white, black):
    """
    В какой цвет окрашиваем квадраты на поле
    :param col: Столбец клетки
    :param row: Строка клетки
    :param white: Первый цвет клеток
    :param black: Второй цвет клеток
    :return: Какого цвета данная клктка
    """
    return white if (row + col) % 2 == 0 else black


def board_gui(margin, width, height, white, black):
    """
    Рисуем черные и белые прямоугольники доски на окне
    :param margin: Отступ
    :param width: Ширина
    :param height: Высота
    :param white: Первый цвет клеток
    :param black: Второй цвет клеток
    """
    for boardRow in range(BOARDSIZE):
        for boardColumn in range(BOARDSIZE):
            xCoordinate = ((margin + width) * boardColumn + margin) + xDistanceFromEdge
            yCoordinate = (margin + height) * boardRow + margin
            currentColour = square_colour(boardColumn, boardRow, white, black)
            pygame.draw.rect(
                screen, currentColour, [xCoordinate, yCoordinate, width, height]
            )


def clear_desk(desk):
    """
    Чистим табличку
    :param desk: Массив хронящий клетки из которых можно убить шашку оппонента
    :return: Этотже массив но уже пустой
    """
    for x in range(BOARDSIZE):
        for y in range(BOARDSIZE):
            desk[x][y] = None

    return desk


def new_game_board(gameBoard, black, white):
    """
    Обнуляем доску и вносим стартовые положения шашек
    :param gameBoard: Массив игрового поля
    :param black: Второй цвет клеток
    :param white: Первый цвет клеток
    """
    gameBoard = clear_desk(gameBoard)

    for x in range(BOARDSIZE):
        for y in range(BOARDSIZE):
            if square_colour(x, y, white, black) == black:
                if y in range(CHESSROW):
                    gameBoard[x][y] = "NormalBlack"
                if y in range(BOARDSIZE - CHESSROW, BOARDSIZE):
                    gameBoard[x][y] = "NormalRed"


def draw_pieces(gameBoard, black, red, margin, width, height, white, radius):
    """
    Рисуем шашки на доске
    :param gameBoard: Массив игрового поля
    :param black: Первый цвет шашек
    :param red: Второй цвет шашек
    :param margin: Отступ
    :param width: Ширина
    :param height: Высота
    :param white: Цвет Обвотки для пешек первого игрока
    :param radius: Радиус самих шашек
    """
    # проходимся по всей таблице, где есть шашки - рисуем
    for x in range(BOARDSIZE):
        for y in range(BOARDSIZE):

            # определение центра круга
            xCoordinate = (
                (margin + width) * x + margin + width // 2
            ) + xDistanceFromEdge
            yCoordinate = (margin + height) * y + margin + height // 2

            # если обычная шашка(не дамка) -> через тернарник пишем вывод на оба цвета
            if gameBoard[x][y] == "NormalBlack" or gameBoard[x][y] == "NormalRed":
                # Рисуем круг
                pygame.draw.circle(
                    screen,
                    black if gameBoard[x][y] == "NormalBlack" else red,
                    (xCoordinate, yCoordinate),
                    radius,
                )

                # Рисуем окружность (обводку)
                pygame.draw.circle(
                    screen,
                    # Цвет обвотки: белый/зеленный
                    white if gameBoard[x][y] == "NormalBlack" else (0, 255, 0),
                    (xCoordinate, yCoordinate),
                    radius,
                    1,
                )

            # Дамка черная
            if gameBoard[x][y] == "KingBlack":
                pygame.draw.circle(screen, black, (xCoordinate, yCoordinate), radius)
                pygame.draw.rect(
                    screen,
                    red,
                    [
                        xCoordinate - radius / (2 ** (0.5)) + 1,
                        yCoordinate - radius / (2 ** (0.5)) + 1,
                        radius * (2 ** 0.5) - 2,
                        radius * (2 ** 0.5) - 2,
                    ],
                )

            # Дамка красная
            if gameBoard[x][y] == "KingRed":
                pygame.draw.circle(screen, red, (xCoordinate, yCoordinate), radius)
                pygame.draw.rect(
                    screen,
                    black,
                    [
                        xCoordinate - radius / (2 ** (0.5)) + 1,
                        yCoordinate - radius / (2 ** (0.5)) + 1,
                        radius * (2 ** 0.5) - 2,
                        radius * (2 ** 0.5) - 2,
                    ],
                )


def draw_frame_und_motion(gameBoard, coll, row, player, margin, width, height, killer):
    """
    Рисуем рамочку вокруг нажатой клетки
    :param gameBoard: Массив игрового поля
    :param coll: Столбец клетки
    :param row: Строка клетки
    :param player: Какой игрок ходил сейчас
    :param margin: Отступ
    :param width: Ширина
    :param height: Высота
    :param killer: Переменная говорящая является это шашка той, что должна рубить
    """
    # Определяем координаты нажатой клетки и тип шашки
    xCoordinate = ((margin + width) * coll + margin) + xDistanceFromEdge
    yCoordinate = (margin + height) * row + margin
    normal_chess = "NormalBlack" if player == "black" else "NormalRed"
    king_chess = "KingBlack" if player == "black" else "KingRed"

    # Проверяем нажалили на клетку и была ли там шашка и если да то рисуем рамку
    if row in range(BOARDSIZE) and coll in range(BOARDSIZE):
        if gameBoard[coll][row] == normal_chess or gameBoard[coll][row] == king_chess:
            pygame.draw.rect(
                screen,
                (255, 255, 0),
                [xCoordinate, yCoordinate, width, height],
                BOARDSIZE,
            )

            # Удаляем с доски лишние(оставшиеся возможности хода)
            for x in range(BOARDSIZE):
                for y in range(BOARDSIZE):
                    if type(gameBoard[x][y]) == list:
                        gameBoard[x][y] = None

            # Рисуем возможности хода
            if gameBoard[coll][row] == normal_chess:
                draw_motion_normal_chess(
                    gameBoard, coll, row, player, margin, width, height, killer
                )
            else:
                draw_motion_king_chess(
                    gameBoard, coll, row, player, margin, width, height, killer
                )


def draw_motion_king_chess(gameBoard, coll, row, player, margin, width, height, killer):
    """
    Добавляем в таблицу куда может ходить дамка
    :param gameBoard: Массив игрового поля
    :param coll: Столбец клетки
    :param row: Строка клетки
    :param player: Какой игрок ходил сейчас
    :param margin: Отступ
    :param width: Ширина
    :param height: Высота
    :param killer: Переменная говорящая является это шашка той, что должна рубить
    """
    difference = coll - row
    secondaryDiff = coll + row - (BOARDSIZE + 1)
    opp_normal_chess = "NormalRed" if player == "black" else "NormalBlack"
    opp_king_chess = "KingRed" if player == "black" else "KingBlack"
    normal_chess = "NormalBlack" if player == "black" else "NormalRed"
    king_chess = "KingBlack" if player == "black" else "KingRed"

    opposite = []

    # главная диагональ вправо
    for x in range(coll, BOARDSIZE):
        try:
            if x - difference < 0:
                continue
            if (
                gameBoard[x][x - difference] == opp_king_chess
                or gameBoard[x][x - difference] == opp_normal_chess
            ):
                if not opposite:
                    opposite = [x, x - difference]
                else:
                    break

            if (
                gameBoard[x][x - difference] == normal_chess
                or gameBoard[x][x - difference] == king_chess
            ) and x != coll:
                break

            if gameBoard[x][x - difference] == None:
                if killer == True:
                    if opposite:
                        gameBoard[x][x - difference] = [[coll, row], opposite]
                        xCoordinate = (
                            (margin + width) * x + margin + width // 2
                        ) + xDistanceFromEdge
                        yCoordinate = (
                            (margin + height) * (x - difference) + margin + height // 2
                        )
                        pygame.draw.circle(
                            screen, (0, 255, 0), (xCoordinate, yCoordinate), 5
                        )
                else:
                    gameBoard[x][x - difference] = [[coll, row], opposite]
                    xCoordinate = (
                        (margin + width) * x + margin + width // 2
                    ) + xDistanceFromEdge
                    yCoordinate = (
                        (margin + height) * (x - difference) + margin + height // 2
                    )
                    pygame.draw.circle(
                        screen, (0, 255, 0), (xCoordinate, yCoordinate), 5
                    )

        except LookupError:
            continue

    opposite = []

    # идем по побочной диагонали вправо
    for x in range(coll, BOARDSIZE):
        try:
            if (BOARDSIZE + 1) + secondaryDiff - x < 0:
                continue
            if (
                gameBoard[x][(BOARDSIZE + 1) + secondaryDiff - x] == opp_king_chess
                or gameBoard[x][(BOARDSIZE + 1) + secondaryDiff - x] == opp_normal_chess
            ):
                if not opposite:
                    opposite = [x, (BOARDSIZE + 1) + secondaryDiff - x]
                else:
                    break

            if (
                gameBoard[x][(BOARDSIZE + 1) + secondaryDiff - x] == normal_chess
                or gameBoard[x][(BOARDSIZE + 1) + secondaryDiff - x] == king_chess
            ) and x != coll:
                break

            if gameBoard[x][(BOARDSIZE + 1) + secondaryDiff - x] == None:
                if killer == True:
                    if opposite:
                        gameBoard[x][(BOARDSIZE + 1) + secondaryDiff - x] = [
                            [coll, row],
                            opposite,
                        ]
                        xCoordinate = (
                            (margin + width) * x + margin + width // 2
                        ) + xDistanceFromEdge
                        yCoordinate = (
                            (margin + height) * ((BOARDSIZE + 1) + secondaryDiff - x)
                            + margin
                            + height // 2
                        )
                        pygame.draw.circle(
                            screen, (0, 255, 0), (xCoordinate, yCoordinate), 5
                        )
                else:
                    gameBoard[x][(BOARDSIZE + 1) + secondaryDiff - x] = [
                        [coll, row],
                        opposite,
                    ]
                    xCoordinate = (
                        (margin + width) * x + margin + width // 2
                    ) + xDistanceFromEdge
                    yCoordinate = (
                        (margin + height) * ((BOARDSIZE + 1) + secondaryDiff - x)
                        + margin
                        + height // 2
                    )
                    pygame.draw.circle(
                        screen, (0, 255, 0), (xCoordinate, yCoordinate), 5
                    )

        except LookupError:
            continue

    opposite = []

    # главная диагональ влево
    for x in range(coll, 0, -1):
        try:
            if x - difference < 0:
                continue

            if (
                gameBoard[x][x - difference] == opp_king_chess
                or gameBoard[x][x - difference] == opp_normal_chess
            ):
                if not opposite:
                    opposite = [x, x - difference]
                else:
                    break

            if (
                gameBoard[x][x - difference] == normal_chess
                or gameBoard[x][x - difference] == king_chess
            ) and x != coll:
                break

            if gameBoard[x][x - difference] == None:
                if killer == True:
                    if opposite:
                        gameBoard[x][x - difference] = [[coll, row], opposite]
                        xCoordinate = (
                            (margin + width) * x + margin + width // 2
                        ) + xDistanceFromEdge
                        yCoordinate = (
                            (margin + height) * (x - difference) + margin + height // 2
                        )
                        pygame.draw.circle(
                            screen, (0, 255, 0), (xCoordinate, yCoordinate), 5
                        )
                else:
                    gameBoard[x][x - difference] = [[coll, row], opposite]
                    xCoordinate = (
                        (margin + width) * x + margin + width // 2
                    ) + xDistanceFromEdge
                    yCoordinate = (
                        (margin + height) * (x - difference) + margin + height // 2
                    )
                    pygame.draw.circle(
                        screen, (0, 255, 0), (xCoordinate, yCoordinate), 5
                    )

        except LookupError:
            continue

    opposite = []

    # идем по побочной диагонали влево
    for x in range(coll, -1, -1):
        try:
            if (BOARDSIZE + 1) + secondaryDiff - x < 0:
                continue
            if (
                gameBoard[x][(BOARDSIZE + 1) + secondaryDiff - x] == opp_king_chess
                or gameBoard[x][(BOARDSIZE + 1) + secondaryDiff - x] == opp_normal_chess
            ):
                if not opposite:
                    opposite = [x, (BOARDSIZE + 1) + secondaryDiff - x]
                else:
                    break

            if (
                gameBoard[x][(BOARDSIZE + 1) + secondaryDiff - x] == normal_chess
                or gameBoard[x][(BOARDSIZE + 1) + secondaryDiff - x] == king_chess
            ) and x != coll:
                break

            if gameBoard[x][(BOARDSIZE + 1) + secondaryDiff - x] == None:
                if killer == True:
                    if opposite:
                        gameBoard[x][(BOARDSIZE + 1) + secondaryDiff - x] = [
                            [coll, row],
                            opposite,
                        ]
                        xCoordinate = (
                            (margin + width) * x + margin + width // 2
                        ) + xDistanceFromEdge
                        yCoordinate = (
                            (margin + height) * ((BOARDSIZE + 1) + secondaryDiff - x)
                            + margin
                            + height // 2
                        )
                        pygame.draw.circle(
                            screen, (0, 255, 0), (xCoordinate, yCoordinate), 5
                        )
                else:
                    gameBoard[x][(BOARDSIZE + 1) + secondaryDiff - x] = [
                        [coll, row],
                        opposite,
                    ]
                    xCoordinate = (
                        (margin + width) * x + margin + width // 2
                    ) + xDistanceFromEdge
                    yCoordinate = (
                        (margin + height) * ((BOARDSIZE + 1) + secondaryDiff - x)
                        + margin
                        + height // 2
                    )
                    pygame.draw.circle(
                        screen, (0, 255, 0), (xCoordinate, yCoordinate), 5
                    )

        except LookupError:
            continue


def draw_motion_normal_chess(
    gameBoard, coll, row, player, margin, width, height, killer
):
    """
    Добавляем в таблицу куда может ходить выбранная пешка (не дамка)
    :param gameBoard: Массив игрового поля
    :param coll: Столбец клетки
    :param row: Строка клетки
    :param player: Какой игрок ходил сейчас
    :param margin: Отступ
    :param width: Ширина
    :param height: Высота
    :param killer: Переменная говорящая является это шашка той, что должна рубить
    """
    opp_normal_chess = "NormalRed" if player == "black" else "NormalBlack"
    opp_king_chess = "KingRed" if player == "black" else "KingBlack"

    # влево
    if coll - 1 in range(BOARDSIZE):

        # вверх
        if row - 1 in range(BOARDSIZE):

            # enemys ahead
            if (
                (
                    gameBoard[coll - 1][row - 1] == opp_normal_chess
                    or gameBoard[coll - 1][row - 1] == opp_king_chess
                )
                and (row - 2) in range(BOARDSIZE)
                and (coll - 2) in range(BOARDSIZE)
                and gameBoard[coll - 2][row - 2] == None
            ):
                gameBoard[coll - 2][row - 2] = [[coll, row], [coll - 1, row - 1]]
                xCoordinate = (
                    (margin + width) * (coll - 2) + margin + width // 2
                ) + xDistanceFromEdge
                yCoordinate = (margin + height) * (row - 2) + margin + height // 2
                pygame.draw.circle(screen, (0, 255, 0), (xCoordinate, yCoordinate), 5)

            # ходить вверх могут только красные, поэтому normalRed
            if (
                gameBoard[coll - 1][row - 1] == None
                and gameBoard[coll][row] == "NormalRed"
                and killer == False
            ):
                gameBoard[coll - 1][row - 1] = [[coll, row], []]
                xCoordinate = (
                    (margin + width) * (coll - 1) + margin + width // 2
                ) + xDistanceFromEdge
                yCoordinate = (margin + height) * (row - 1) + margin + height // 2
                pygame.draw.circle(screen, (0, 255, 0), (xCoordinate, yCoordinate), 5)

        # вниз
        if row + 1 in range(BOARDSIZE):

            # enemys ahead
            if (
                (
                    gameBoard[coll - 1][row + 1] == opp_normal_chess
                    or gameBoard[coll - 1][row + 1] == opp_king_chess
                )
                and (row + 2) in range(BOARDSIZE)
                and (coll - 2) in range(BOARDSIZE)
                and gameBoard[coll - 2][row + 2] == None
            ):
                gameBoard[coll - 2][row + 2] = [[coll, row], [coll - 1, row + 1]]
                xCoordinate = (
                    (margin + width) * (coll - 2) + margin + width // 2
                ) + xDistanceFromEdge
                yCoordinate = (margin + height) * (row + 2) + margin + height // 2
                pygame.draw.circle(screen, (0, 255, 0), (xCoordinate, yCoordinate), 5)

            # ходить вниз могут только черные, поэтому normalBlack
            if (
                gameBoard[coll - 1][row + 1] == None
                and gameBoard[coll][row] == "NormalBlack"
                and killer == False
            ):
                gameBoard[coll - 1][row + 1] = [[coll, row], []]
                xCoordinate = (
                    (margin + width) * (coll - 1) + margin + width // 2
                ) + xDistanceFromEdge
                yCoordinate = (margin + height) * (row + 1) + margin + height // 2
                pygame.draw.circle(screen, (0, 255, 0), (xCoordinate, yCoordinate), 5)

    # вправо
    if coll + 1 in range(BOARDSIZE):

        # вверх
        if row - 1 in range(BOARDSIZE):

            # enemys ahead
            if (
                (
                    gameBoard[coll + 1][row - 1] == opp_normal_chess
                    or gameBoard[coll + 1][row - 1] == opp_king_chess
                )
                and (row - 2) in range(BOARDSIZE)
                and (coll + 2) in range(BOARDSIZE)
                and gameBoard[coll + 2][row - 2] == None
            ):
                gameBoard[coll + 2][row - 2] = [[coll, row], [coll + 1, row - 1]]
                xCoordinate = (
                    (margin + width) * (coll + 2) + margin + width // 2
                ) + xDistanceFromEdge
                yCoordinate = (margin + height) * (row - 2) + margin + height // 2
                pygame.draw.circle(screen, (0, 255, 0), (xCoordinate, yCoordinate), 5)

            if (
                gameBoard[coll + 1][row - 1] == None
                and gameBoard[coll][row] == "NormalRed"
                and killer == False
            ):
                gameBoard[coll + 1][row - 1] = [[coll, row], []]
                xCoordinate = (
                    (margin + width) * (coll + 1) + margin + width // 2
                ) + xDistanceFromEdge
                yCoordinate = (margin + height) * (row - 1) + margin + height // 2
                pygame.draw.circle(screen, (0, 255, 0), (xCoordinate, yCoordinate), 5)

        # вниз
        if row + 1 in range(BOARDSIZE):

            # enemys ahead
            if (
                (
                    gameBoard[coll + 1][row + 1] == opp_normal_chess
                    or gameBoard[coll + 1][row + 1] == opp_king_chess
                )
                and (row + 2) in range(BOARDSIZE)
                and (coll + 2) in range(BOARDSIZE)
                and gameBoard[coll + 2][row + 2] == None
            ):
                gameBoard[coll + 2][row + 2] = [[coll, row], [coll + 1, row + 1]]
                xCoordinate = (
                    (margin + width) * (coll + 2) + margin + width // 2
                ) + xDistanceFromEdge
                yCoordinate = (margin + height) * (row + 2) + margin + height // 2
                pygame.draw.circle(screen, (0, 255, 0), (xCoordinate, yCoordinate), 5)

            if (
                gameBoard[coll + 1][row + 1] == None
                and gameBoard[coll][row] == "NormalBlack"
                and killer == False
            ):
                gameBoard[coll + 1][row + 1] = [[coll, row], []]
                xCoordinate = (
                    (margin + width) * (coll + 1) + margin + width // 2
                ) + xDistanceFromEdge
                yCoordinate = (margin + height) * (row + 1) + margin + height // 2
                pygame.draw.circle(screen, (0, 255, 0), (xCoordinate, yCoordinate), 5)


def check_king(gameBoard):
    """
    Проверка на дамку
    :param gameBoard: Массив игрового поля
    """
    for x in range(BOARDSIZE):
        if gameBoard[x][0] == "NormalRed":
            gameBoard[x][0] = "KingRed"
        if gameBoard[x][(BOARDSIZE - 1)] == "NormalBlack":
            gameBoard[x][(BOARDSIZE - 1)] = "KingBlack"


def reload_kill(kill_board, gameBoard, player): # проходит ли она по всем пешкам
    """
    Делаем таблицу килов заново
    :param kill_board: Массив хронящий клетки из которых можно убить шашку оппонента
    :param gameBoard: Массив игрового поля
    :param player: Какой игрок ходит сейчас
    :param margin: Отступ
    :param width: Ширина
    :param height: Висота
    :return: Наш заполненый массив хронящий клетки из которых можно убить шашку оппонента
    """
    kill_board = clear_desk(kill_board)
    normal_chess = "NormalBlack" if player == "black" else "NormalRed"
    king_chess = "KingBlack" if player == "black" else "KingRed"
    opp_normal_chess = "NormalRed" if player == "black" else "NormalBlack"
    opp_king_chess = "KingRed" if player == "black" else "KingBlack"

    for coll in range(BOARDSIZE):
        for row in range(BOARDSIZE):
            if gameBoard[coll][row] == normal_chess:
                if (
                    (coll - 1) in range(BOARDSIZE)
                    and (row - 1) in range(BOARDSIZE)
                    and (
                        gameBoard[coll - 1][row - 1] == opp_normal_chess
                        or gameBoard[coll - 1][row - 1] == opp_king_chess
                    )
                    and (row - 2) in range(BOARDSIZE)
                    and (coll - 2) in range(BOARDSIZE)
                    and gameBoard[coll - 2][row - 2] == None
                ):
                    kill_board[coll][row] = True

                if (
                    (coll - 1) in range(BOARDSIZE)
                    and (row + 1) in range(BOARDSIZE)
                    and (
                        gameBoard[coll - 1][row + 1] == opp_normal_chess
                        or gameBoard[coll - 1][row + 1] == opp_king_chess
                    )
                    and (row + 2) in range(BOARDSIZE)
                    and (coll - 2) in range(BOARDSIZE)
                    and gameBoard[coll - 2][row + 2] == None
                ):
                    kill_board[coll][row] = True

                if (
                    (coll + 1) in range(BOARDSIZE)
                    and (row - 1) in range(BOARDSIZE)
                    and (
                        gameBoard[coll + 1][row - 1] == opp_normal_chess
                        or gameBoard[coll + 1][row - 1] == opp_king_chess
                    )
                    and (row - 2) in range(BOARDSIZE)
                    and (coll + 2) in range(BOARDSIZE)
                    and gameBoard[coll + 2][row - 2] == None
                ):
                    kill_board[coll][row] = True

                if (
                    (coll + 1) in range(BOARDSIZE)
                    and (row + 1) in range(BOARDSIZE)
                    and (
                        gameBoard[coll + 1][row + 1] == opp_normal_chess
                        or gameBoard[coll + 1][row + 1] == opp_king_chess
                    )
                    and (row + 2) in range(BOARDSIZE)
                    and (coll + 2) in range(BOARDSIZE)
                    and gameBoard[coll + 2][row + 2] == None
                ):
                    kill_board[coll][row] = True

            elif gameBoard[coll][row] == king_chess:
                opposite = []
                difference = coll - row
                secondaryDiff = coll + row - (BOARDSIZE + 1)

                # главная диагональ вправо
                for x in range(coll, BOARDSIZE):
                    try:
                        if x - difference < 0:
                            continue
                        if (
                            gameBoard[x][x - difference] == opp_king_chess
                            or gameBoard[x][x - difference] == opp_normal_chess
                        ):
                            if not opposite:
                                opposite = [x, x - difference]
                            else:
                                break

                        if (
                            gameBoard[x][x - difference] == normal_chess
                            or gameBoard[x][x - difference] == king_chess
                        ) and x != coll:
                            break

                        if gameBoard[x][x - difference] == None and opposite:
                            kill_board[coll][row] = True
                            break

                    except LookupError:
                        continue

                opposite = []

                # идем по побочной диагонали вправо
                for x in range(coll, BOARDSIZE):
                    try:
                        if (BOARDSIZE + 1) + secondaryDiff - x < 0:
                            continue
                        if (
                            gameBoard[x][(BOARDSIZE + 1) + secondaryDiff - x]
                            == opp_king_chess
                            or gameBoard[x][(BOARDSIZE + 1) + secondaryDiff - x]
                            == opp_normal_chess
                        ):
                            if not opposite:
                                opposite = [x, (BOARDSIZE + 1) + secondaryDiff - x]
                            else:
                                break

                        if (
                            gameBoard[x][(BOARDSIZE + 1) + secondaryDiff - x]
                            == normal_chess
                            or gameBoard[x][(BOARDSIZE + 1) + secondaryDiff - x]
                            == king_chess
                        ) and x != coll:
                            break

                        if (
                            gameBoard[x][(BOARDSIZE + 1) + secondaryDiff - x] == None
                            and opposite
                        ):
                            kill_board[coll][row] = True
                            break

                    except LookupError:
                        continue

                opposite = []

                # главная диагональ влево
                for x in range(coll, 0, -1):
                    try:
                        if x - difference < 0:
                            continue

                        if (
                            gameBoard[x][x - difference] == opp_king_chess
                            or gameBoard[x][x - difference] == opp_normal_chess
                        ):
                            if not opposite:
                                opposite = [x, x - difference]
                            else:
                                break

                        if (
                            gameBoard[x][x - difference] == normal_chess
                            or gameBoard[x][x - difference] == king_chess
                        ) and x != coll:
                            break

                        if gameBoard[x][x - difference] == None and opposite:
                            kill_board[coll][row] = True
                            break
                    except LookupError:
                        continue

                opposite = []

                # идем по побочной диагонали влево
                for x in range(coll, -1, -1):
                    try:
                        if (BOARDSIZE + 1) + secondaryDiff - x < 0:
                            continue
                        if (
                            gameBoard[x][(BOARDSIZE + 1) + secondaryDiff - x]
                            == opp_king_chess
                            or gameBoard[x][(BOARDSIZE + 1) + secondaryDiff - x]
                            == opp_normal_chess
                        ):
                            if not opposite:
                                opposite = [x, (BOARDSIZE + 1) + secondaryDiff - x]
                            else:
                                break

                        if (
                            gameBoard[x][(BOARDSIZE + 1) + secondaryDiff - x]
                            == normal_chess
                            or gameBoard[x][(BOARDSIZE + 1) + secondaryDiff - x]
                            == king_chess
                        ) and x != coll:
                            break

                        if (
                            gameBoard[x][(BOARDSIZE + 1) + secondaryDiff - x] == None
                            and opposite
                        ):
                            kill_board[coll][row] = True
                            break

                    except LookupError:
                        continue

    return kill_board


def check_all_kill(kill_board):
    """
    Проверка что есть хотя бы 1 true
    :param kill_board:
    :return: Bool
    """
    for x in range(BOARDSIZE):
        for y in range(BOARDSIZE):
            if kill_board[x][y] == True:
                return True

    return False
