"""
Сервер шашек. Заносит каждого клиента в массив.
Получает пакет от одного клиента и отправляет его паре.
"""

import json
import socket
import threading

# Создаем константы и массив, который будет хранить местоположение пешек
LOCALHOST = "127.0.0.1"
PORT = 1488
black = (45, 19, 0)
white = (226, 211, 180)
BOARDSIZE = 8
CHESSROW = 3
stockGameBoard = [[None] * BOARDSIZE for _ in range(BOARDSIZE)]

# Заполняем массив пешек
for x in range(BOARDSIZE):
    for y in range(BOARDSIZE):
        if (x + y) % 2 == 1:
            if y in range(CHESSROW):
                stockGameBoard[x][y] = "NormalBlack"
            if y in range(BOARDSIZE - CHESSROW, BOARDSIZE):
                stockGameBoard[x][y] = "NormalRed"

#
ClientArr = []

# Создаем сокет для сервера
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

# Говорим серверу с каким хостом и портом ему работать
server.bind((LOCALHOST, PORT))
print("serv start")

#
class ClientThread(threading.Thread):

    """
    Класс потока для каджого клиента
    """

    def __init__(self, __client_address, clientsocket):
        threading.Thread.__init__(self)
        self.csocket = clientsocket
        print("Новое подключение: ", __client_address)

    # пробуем вернуть себя
    def get_self(self):

        """
        возвращаем self клиента, чтобы потом сервер мог отсылать сообщения другим клиентам
        """

        return self

    # Обработка полученых сообщений
    def run(self):
        msg = ""
        while True:
            try:
                data = self.csocket.recv(4096)
            except ConnectionError:
                print("клиент отключился")
                break

            # данные приходят от клиента
            msg = data.decode()
            if msg == "":
                ClientArr.remove(self)
                print("Отключение")
                break
            if msg != "hi":
                try:
                    try:
                        if ClientArr.index(self) % 2 == 0:
                            ClientArr[ClientArr.index(self) + 1].csocket.send(
                                bytes(msg, "UTF-8")
                            )
                        else:
                            ClientArr[ClientArr.index(self) - 1].csocket.send(
                                bytes(msg, "UTF-8")
                            )
                    except ValueError:
                        print("нет пользователя")
                except IndexError:
                    print("ы")


#
while True:
    server.listen(1)
    clientsock, client_address = server.accept()
    newthread = ClientThread(client_address, clientsock)
    newthread.start()
    ClientArr.append(newthread.get_self())
    if ClientArr.index(newthread.get_self()) % 2 == 0:
        newthread.get_self().csocket.send(
            bytes(json.dumps([stockGameBoard, "red", True]), "UTF-8")
        )
    else:
        newthread.get_self().csocket.send(
            bytes(json.dumps([stockGameBoard, "black", False]), "UTF-8")
        )
    print(ClientArr)
