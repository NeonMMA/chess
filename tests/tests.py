from all_function import change_player
from all_function import square_colour
from all_function import clear_desk
from all_function import check_all_kill
from all_function import reload_kill
import pytest


@pytest.mark.parametrize("player, changed_player", [("red", "black"), ("black", "red")])
def test_change_player(player, changed_player):
    """
    Проверка стандартных вариантов.
    """
    assert change_player(player) == changed_player


@pytest.mark.parametrize("player", [5, ""])
def test_change_player_with_type_error(player):
    """
    Функция возвращает всегда черный, если входной параметр не черный, поэтому ошибок не будет.
    """
    assert change_player(player) == "black"


@pytest.mark.parametrize("x, y, result", [(-1, -1, "white"), (1, 3, "white"), (0.5, 0.3, "black")])
def test_square_colour(x, y, result):
    assert square_colour(x, y, "white", "black") == result


@pytest.mark.parametrize("x, y, result", [(-0.8, -0.4, "black"), (-0.2, 0.7, "black"), (0.5, 0.3, "black")])
def test_square_colour_with_float_parameters(x, y, result):
    """
    Если не четная сумма (остаток от деления на 2 не равен 0), то черный.
    Дробная сумма остаток ноль не дает -> черный
    """
    assert square_colour(x, y, "white", "black") == result


@pytest.mark.parametrize("x, y", [(-0.8, "sdf"), (" asfd", 0.7), ("first", "second"), ("", "")])
def test_square_colour_with_type_error(x, y):
    """
    Ошибка типа входных данных (дана строка вместо числа)
    """
    with pytest.raises(TypeError):
        square_colour(x, y, "white", "black")


@pytest.mark.parametrize("boardSize", [-1, 4, 7])
def test_clear_desk_with_index_error(boardSize):
    """
    На вход в функцию подаем таблицу заполненную 1, тк нам не важно что в ней лежало
    Проход по массиву внутри функции осуществляется в пределах BOARDSIZE ->
    выход за границы массива при меньших размерах killDesk.
    """
    with pytest.raises(IndexError):
        assert clear_desk([[1] * boardSize for _ in range(boardSize)]) == [[None] * boardSize for _ in range(boardSize)]


@pytest.mark.parametrize("boardSize", [9, 10, 20])
def test_clear_desk_with_error(boardSize):
    """
    Если размеры kill_desk превышают BOARDSIZE
    """
    assert clear_desk([[1] * boardSize for _ in range(boardSize)]) != [[None] * boardSize for _ in range(boardSize)]


def test_clear_desk_good():
    """
    Единственный правильный вариант, когда размер совпадает с BOARDSIZE (сейчас это 8)
    """
    assert clear_desk([[1] * 8 for _ in range(8)]) == [[None] * 8 for _ in range(8)]


def test_check_all_kill_none():
    """
    Проверка пустой таблицы.
    """
    kill_board = [[None] * 8 for _ in range(8)]
    assert not check_all_kill(kill_board)


def test_check_all_kill_white_cell():
    """
    Проверка если True стоит на белом поле (такого быть не может если правильно заполнена kill_desk)
    функция возвращает True (по логике-то не должна)
    """
    kill_board = [[None] * 8 for _ in range(8)]
    kill_board[0][0] = True
    assert check_all_kill(kill_board)


def test_check_all_kill_cell_type_error():
    """
    Проверка если в таблице лежит другой тип данных
    Функция вернет False тк проверяет только наличие True
    """
    kill_board = [[None] * 8 for _ in range(8)]
    kill_board[1][4] = "mark"
    assert not check_all_kill(kill_board)


def test_check_all_kill_type_error():
    """
    Проверка если в функцию подали другой тип данных
    Ошибка появляется еще до проверки ячейки тк не существует индекса -> IndexError
    """
    kill_board = "mark"
    with pytest.raises(IndexError):
        check_all_kill(kill_board)


def test_check_all_kill_good():
    """
    Проверка пустой таблицы.
    """
    kill_board = [[None] * 8 for _ in range(8)]
    kill_board[1][4] = True
    assert check_all_kill(kill_board)


@pytest.mark.parametrize("player", ["black", "red"])
def test_reload_kill_normal_good(player):
    """
        Проверяем нормальный ход у обычных
    """
    gameBoard = [[None] * 8 for _ in range(8)]
    gameBoard[4][5] = "NormalRed"
    gameBoard[5][4] = "NormalBlack"
    kill_board = [[None] * 8 for _ in range(8)]
    end_kill_board = [[None] * 8 for _ in range(8)]
    if player == "red":
        end_kill_board[4][5] = True
    elif player == "black":
        end_kill_board[5][4] = True
    assert reload_kill(kill_board, gameBoard, player) == end_kill_board


@pytest.mark.parametrize("player", ["black", "red"])
def test_reload_kill_king_good(player):
    """
    Проверяем нормальный ход у дамок
    """
    gameBoard = [[None] * 8 for _ in range(8)]
    gameBoard[4][5] = "KingRed"
    gameBoard[5][4] = "KingBlack"
    kill_board = [[None] * 8 for _ in range(8)]
    end_kill_board = [[None] * 8 for _ in range(8)]

    # задаем ячейку true в проверочной таблице
    if player == "red":
        end_kill_board[4][5] = True
    elif player == "black":
        end_kill_board[5][4] = True
    assert reload_kill(kill_board, gameBoard, player) == end_kill_board


@pytest.mark.parametrize("player", ["", "sdf", 5])
def test_reload_kill_with_player_error(player):
    """
    При невалидном игроке функция не отваливается, тк проверяет на равенство с black, остальное - игрок red
    """
    gameBoard = [[None] * 8 for _ in range(8)]
    gameBoard[4][5] = "NormalRed"
    gameBoard[5][4] = "NormalBlack"
    kill_board = [[None] * 8 for _ in range(8)]
    end_kill_board = [[None] * 8 for _ in range(8)]
    end_kill_board[4][5] = True
    assert reload_kill(kill_board, gameBoard, player) == end_kill_board
